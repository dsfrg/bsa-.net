export interface UserReset {
    email: string;
    oldPassword: string;
    newPassword: string;
}
