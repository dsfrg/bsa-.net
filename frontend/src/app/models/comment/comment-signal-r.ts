import { User } from '../user';
import { Reaction } from '../reactions/reaction';

export interface CommentSignalR {
    id: number;
    createdAt: Date;
    author: User;
    body: string;
    postId: number;
    reactions: Reaction[];
}
