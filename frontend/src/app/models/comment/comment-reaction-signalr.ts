import { User } from '../user';
import { Reaction } from '../reactions/reaction';

export interface CommentReactionSignalr {
    id: number;
    Author: User;
    postId: number;
    reactions: Reaction[];
}
