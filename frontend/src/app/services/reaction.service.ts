import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PostService } from './post.service';
import { DialogType } from '../models/common/auth-dialog-type';
import { UserReactionDialogComponent } from '../components/user-reaction-dialog/user-reaction-dialog.component';
import { LikedUser } from '../models/auth/liked-user';

@Injectable({
  providedIn: 'root'
})
export class ReactionService {

  constructor(private dialog: MatDialog, private postService: PostService) { }

  public openDialog(users: LikedUser[], name: string) {
    const dialog = this.dialog.open(UserReactionDialogComponent, {
        data: { LikedUser: users, Name:  name},
        minWidth: 400,
        minHeight: 400,
        autoFocus: true,
        backdropClass: 'dialog-backdrop',
        position: {
            top: '0'
        }
    });
  }
}
