import { Injectable, EventEmitter } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Comment } from '../models/comment/comment';
import { CommentService } from './comment.service';

@Injectable({ providedIn: 'root' })
export class LikeService {
    // public postLikedRemovedEvent: EventEmitter<boolean> = new EventEmitter();
    public constructor(private authService: AuthenticationService, private postService: PostService, private commentService: CommentService) {}

    public likePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            isDislike: false,
            userId: currentUser.id
        };
        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            const isLiked = innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike;
            if (isLiked === true) {
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
                // this.postLikedRemovedEvent.emit(true);
            }
            else {
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike = true;
            }
        }
        else {
            innerPost.reactions = innerPost.reactions.concat({ isLike: true,  isDislike: false, user: currentUser });
        }
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: true, isDislike: false, user: currentUser });

                return of(innerPost);
            })
        );

    }

    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            isDislike: true,
            userId: currentUser.id
        };

        let hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            const isDisliked = innerPost.reactions.find((x) => x.user.id === currentUser.id).isDislike;
            if (isDisliked === true) {
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
            }
            else {
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isDislike = true;
                innerPost.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
                // this.postLikedRemovedEvent.emit(true);
            }
        }
        else {
            innerPost.reactions = innerPost.reactions.concat({ isLike: false, isDislike: true, user: currentUser });
        }
        hasReaction = innerPost.reactions.some((x) => x.user.id === currentUser.id);

        return this.postService.dislikePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                innerPost.reactions = hasReaction
                    ? innerPost.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerPost.reactions.concat({ isLike: false, isDislike: true, user: currentUser });

                return of(innerPost);
            })
        );

    }

    public likeComment(comment: Comment, currentUser: User) {
        const innnerComment = comment;

        const reaction: NewReaction = {
            entityId: innnerComment.id,
            isLike: true,
            isDislike: false,
            userId: currentUser.id
        };
        let hasReaction = innnerComment.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            const isLiked = innnerComment.reactions.find((x) => x.user.id === currentUser.id).isLike;
            if (isLiked === true) {
                innnerComment.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innnerComment.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
            }
            else {
                innnerComment.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innnerComment.reactions.find((x) => x.user.id === currentUser.id).isLike = true;
            }
        }
        else {
            innnerComment.reactions = innnerComment.reactions.concat({ isLike: true,  isDislike: false, user: currentUser });
        }
        hasReaction = innnerComment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.likePost(reaction).pipe(
            map(() => innnerComment),
            catchError(() => {
                innnerComment.reactions = hasReaction
                    ? innnerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innnerComment.reactions.concat({ isLike: true, isDislike: false, user: currentUser });

                return of(innnerComment);
            })
        );

    }

    public dislikeComment(comment: Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: false,
            isDislike: true,
            userId: currentUser.id
        };

        let hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);
        if (hasReaction) {
            const isDisliked = innerComment.reactions.find((x) => x.user.id === currentUser.id).isDislike;
            if (isDisliked === true) {
                innerComment.reactions.find((x) => x.user.id === currentUser.id).isDislike = false;
                innerComment.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
            }
            else {
                innerComment.reactions.find((x) => x.user.id === currentUser.id).isDislike = true;
                innerComment.reactions.find((x) => x.user.id === currentUser.id).isLike = false;
            }
        }
        else {
            innerComment.reactions = innerComment.reactions.concat({ isLike: false, isDislike: true, user: currentUser });
        }
        hasReaction = innerComment.reactions.some((x) => x.user.id === currentUser.id);

        return this.commentService.dislikePost(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                innerComment.reactions = hasReaction
                    ? innerComment.reactions.filter((x) => x.user.id !== currentUser.id)
                    : innerComment.reactions.concat({ isLike: false, isDislike: true, user: currentUser });

                return of(innerComment);
            })
        );

    }
}
