import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CommentService } from './comment.service';
import { CommentEditDialogComponent } from '../components/comment-edit-dialog/comment-edit-dialog.component';
import { EditComment } from '../models/comment/edit-comment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NewComment } from '../models/comment/new-comment';

@Injectable({
  providedIn: 'root'
})
export class EditCommentDialogService {
  private unsubscribe$ = new Subject<void>();

  constructor(private dialog: MatDialog, private commentService: CommentService) { }

  public openDialog(comment: EditComment) {
    const dialog = this.dialog.open(CommentEditDialogComponent, {
        data: { data:  comment},
        minWidth: 400,
        minHeight: 400,
        autoFocus: true,
        backdropClass: 'dialog-backdrop',
        position: {
            top: '0'
        }
    });

    dialog
    .afterClosed()
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe((result: EditComment) => {
        this.commentService.updateComment(result)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((responce) => {
            this.commentService.commentEdited.emit(responce.body);
        });
    });
  }

}
