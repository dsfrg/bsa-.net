import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Post } from '../models/post/post';
import { CommentSignalR } from '../models/comment/comment-signal-r';

@Injectable({
  providedIn: 'root'
})
export class SignalrEventsService {
  public postReactionsEdited: EventEmitter<Post> = new EventEmitter();
  public commentReactionsEdited: EventEmitter<boolean> = new EventEmitter();
  constructor() { }

}
