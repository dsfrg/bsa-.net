import { Injectable, EventEmitter } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { EditComment } from '../models/comment/edit-comment';
import { NewReaction } from '../models/reactions/newReaction';
import { LikedUser } from '../models/auth/liked-user';

@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';
    public commentDeleted: EventEmitter<number> = new EventEmitter();
    public commentEdited: EventEmitter<Comment> = new EventEmitter();

    constructor(private httpService: HttpInternalService) {}

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public deleteComment(id: number) {
        return this.httpService.deleteFullRequest(`${this.routePrefix}/delete/${id}`);
    }

    public updateComment(update: EditComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/update`, update);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest(`${this.routePrefix}/like`, reaction);
    }
    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest(`${this.routePrefix}/dislike`, reaction);
    }

    public displayWhoLiked(id: number) {
        return this.httpService.getFullRequest<LikedUser[]>(this.routePrefix + '/likeduser/' + id);
    }

    public displayWhoDisliked(id: number) {
        return this.httpService.getFullRequest<LikedUser[]>(this.routePrefix + '/dislikeduser/' + id);
    }
}
