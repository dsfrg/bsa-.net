import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { Post } from '../models/post/post';
import { NewReaction } from '../models/reactions/newReaction';
import { NewPost } from '../models/post/new-post';
import { EditPost } from '../models/post/edit-post';
import { LikedUser } from '../models/auth/liked-user';
import { SharedPostDto } from '../models/post/shared-post-dto';

@Injectable({ providedIn: 'root' })
export class PostService {
    public routePrefix = '/api/posts';


    constructor(private httpService: HttpInternalService) {}

    public getPosts() {
        return this.httpService.getFullRequest<Post[]>(`${this.routePrefix}`);
    }

    public createPost(post: NewPost) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}`, post);
    }

    public likePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/like`, reaction);
    }
    public dislikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Post>(`${this.routePrefix}/dislike`, reaction);
    }

    public editPost(edit: EditPost) {
        return this.httpService.putFullRequest<Post>(`${this.routePrefix}/edit`, edit);
    }

    public deletePost(id: number) {
        return this.httpService.deleteFullRequest(this.routePrefix + '/delete/' + id);
    }

    public displayWhoLiked(id: number) {
        return this.httpService.getFullRequest<LikedUser[]>(this.routePrefix + '/likeduser/' + id);
    }

    public displayWhoDisliked(id: number) {
        return this.httpService.getFullRequest<LikedUser[]>(this.routePrefix + '/dislikeduser/' + id);
    }

    public sharePostByEmail(shared: SharedPostDto) {
        return this.httpService.postFullRequest(`${this.routePrefix}/share`, shared);
    }
}
