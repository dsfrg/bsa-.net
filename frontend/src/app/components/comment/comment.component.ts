import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Comment } from '../../models/comment/comment';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { LikeService } from 'src/app/services/like.service';
import { CommentService } from 'src/app/services/comment.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { EventService } from 'src/app/services/event.service';
import { ImgurService } from 'src/app/services/imgur.service';
import { ReactionService } from 'src/app/services/reaction.service';
import { User } from 'src/app/models/user';
import { Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { EditCommentDialogService } from 'src/app/services/edit-comment-dialog.service';
import { SignalrEventsService } from 'src/app/services/signalr-events.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent implements OnInit {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Input() public postId: number;
    public likeCount: number;
    public dislikeCount: number;
    public updateComment: EditComment = {postId: 0, commentId: 0, Body: ''};
    private unsubscribe$ = new Subject<void>();
    constructor(
        private likeService: LikeService,
        private commentService: CommentService,
        private reactionDialogService: ReactionService,
        private editCommentDialogService: EditCommentDialogService,
        private signalRService: SignalrEventsService
    ) {}

    public ngOnInit() {
        this.likeCount = this.comment.reactions.filter((x) => x.isLike).length;
        this.dislikeCount = this.comment.reactions.filter((x) => x.isDislike).length;

        this.signalRService.commentReactionsEdited
                            .pipe(takeUntil(this.unsubscribe$))
                            .subscribe((response) => {
                                this.likeCount = this.comment.reactions.filter((x) => x.isLike).length;
                                this.dislikeCount = this.comment.reactions.filter((x) => x.isDislike).length;
                            });
    }

    public dislikeComment() {
        this.likeService
        .dislikeComment(this.comment, this.currentUser)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((comment) => {
            this.comment = comment;
            console.log(this.comment);
        });
        this.dislikeCount = this.comment.reactions.filter((x) => x.isDislike).length;
        this.likeCount = this.comment.reactions.filter((x) => x.isLike).length;
    }

    public likeComment() {
        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
            });
        this.likeCount = this.comment.reactions.filter((x) => x.isLike).length;
        this.dislikeCount = this.comment.reactions.filter((x) => x.isDislike).length;
    }

    public displayWhoLiked() {
        this.commentService.displayWhoLiked(this.comment.id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((resp) => {
            this.reactionDialogService.openDialog(resp.body, 'Comment likes');
        });
    }

    public displayWhoDisliked() {
        this.commentService.displayWhoDisliked(this.comment.id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((resp) => {
            this.reactionDialogService.openDialog(resp.body, 'Comment dislikes');
        });
    }

    public editComment() {
        this.updateComment.postId = this.postId;
        this.updateComment.commentId = this.comment.id;
        this.updateComment.Body = this.comment.body;
        console.log(this.updateComment);
        this.editCommentDialogService.openDialog(this.updateComment);
    }

    public deleteComment() {
        this.commentService.deleteComment(this.comment.id)
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe(() => {
            this.commentService.commentDeleted.emit(this.comment.id);
        });
    }
}
