import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserReset } from 'src/app/models/user-reset';
import { ToasterNotificationService } from 'src/app/services/toaster-notification.service';
import { UserService } from 'src/app/services/user.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  public resetForm: FormGroup;
  public userReset = {} as UserReset;

  private unsubscribe$ = new Subject<void>();
  constructor(
    private router: Router,
    private activatedRouter: ActivatedRoute,
    private toastrService: ToasterNotificationService,
    private userService: UserService
    ) { }

  public ngOnInit() {
    this.resetForm = new FormGroup({
      oldPassword: new FormControl(''),
      newPassword: new FormControl(''),
      confirmPassword: new FormControl()
    });
    this.activatedRouter.queryParams.subscribe((p) => {
      this.userReset.email = p.email;
    });
  }

  public ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
}

  public onSubmit() {
    console.log(this.resetForm.value);
    if (this.resetForm.controls['newPassword'].value !== this.resetForm.controls['confirmPassword'].value) {
      this.toastrService.showError('Attention', 'Your new password and confirmatian are not similar');
      return;
    }

    if (this.resetForm.controls['newPassword'].value === '' || this.resetForm.controls['newPassword'].value === null) {
      this.toastrService.showError('Attention', 'Your new password can not be empty');
      return;
    }

    this.userReset.oldPassword = this.resetForm.controls['oldPassword'].value;
    this.userReset.newPassword = this.resetForm.controls['newPassword'].value;
    console.log(this.userReset);
    this.userService.resetPassword(this.userReset)
                    .pipe(takeUntil(this.unsubscribe$))
                    .subscribe((x) => {
                      this.toastrService.showSuccess('Bingo', 'You will be redirected to main page after 3 sec, please log in');

                      setTimeout(() => {
                        this.router.navigate(['/']);
                      }, 3000);
                    }, (error) => {
                      this.toastrService.showError('Error', error);
                    });
  }
}
