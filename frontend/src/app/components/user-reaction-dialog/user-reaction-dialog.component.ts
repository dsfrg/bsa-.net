import { Component, OnInit, Inject } from '@angular/core';
import { LikedUser } from 'src/app/models/auth/liked-user';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-user-reaction-dialog',
  templateUrl: './user-reaction-dialog.component.html',
  styleUrls: ['./user-reaction-dialog.component.css']
})
export class UserReactionDialogComponent implements OnInit {
  public actionName = '';
  public users: LikedUser[];
  // tslint:disable-next-line: no-empty
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  // tslint:disable-next-line: no-empty
  public ngOnInit() {
    this.users = this.data.LikedUser;
    this.actionName = this.data.Name;
  }

}
