import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../models/post/post';
import { User } from '../../models/user';
import { Subject } from 'rxjs';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { AuthenticationService } from '../../services/auth.service';
import { PostService } from '../../services/post.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { DialogType } from '../../models/common/auth-dialog-type';
import { EventService } from '../../services/event.service';
import { ImgurService } from '../../services/imgur.service';
import { NewPost } from '../../models/post/new-post';
import { switchMap, takeUntil } from 'rxjs/operators';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SnackBarService } from '../../services/snack-bar.service';
import { CommentService } from 'src/app/services/comment.service';
import { LikeService } from 'src/app/services/like.service';
import { ToasterNotificationService } from 'src/app/services/toaster-notification.service';
import { SignalrEventsService } from 'src/app/services/signalr-events.service';
import { NewComment } from 'src/app/models/comment/new-comment';
import { Comment } from 'src/app/models/comment/comment';
import { CommentSignalR } from 'src/app/models/comment/comment-signal-r';
import { CommentReactionSignalr } from 'src/app/models/comment/comment-reaction-signalr';

@Component({
    selector: 'app-main-thread',
    templateUrl: './main-thread.component.html',
    styleUrls: ['./main-thread.component.sass']
})
export class MainThreadComponent implements OnInit, OnDestroy {
    public posts: Post[] = [];
    public cachedPosts: Post[] = [];
    public isOnlyMine = false;
    public isLikedByMe = false;

    public currentUser: User;
    public imageUrl: string;
    public imageFile: File;
    public post = {} as NewPost;
    public comment = {} as Comment;
    public showPostContainer = false;
    public loading = false;
    public loadingPosts = false;

    public postHub: HubConnection;
    public reactionHub: HubConnection;
    public commentHub: HubConnection;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private snackBarService: SnackBarService,
        private authService: AuthenticationService,
        private postService: PostService,
        private imgurService: ImgurService,
        private authDialogService: AuthDialogService,
        private eventService: EventService,
        private commentService: CommentService,
        private likeService: LikeService,
        private toasterService: ToasterNotificationService,
        private signalRService: SignalrEventsService
    ) {}

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.postHub.stop();
        this.reactionHub.stop();
        this.commentHub.stop();
    }

    public ngOnInit() {
        this.registerHub();
        this.registerReactionHub();
        this.getPosts();
        this.getUser();

        this.eventService.userChangedEvent$.pipe(takeUntil(this.unsubscribe$)).subscribe((user) => {
            this.currentUser = user;
            this.post.authorId = this.currentUser ? this.currentUser.id : undefined;
        });

        this.commentService.commentEdited
                            .pipe(takeUntil(this.unsubscribe$))
                            .subscribe((response) => {
                                console.log(response.body);
                                this.posts
                                        .find((x) => x.comments.find((c) => c.id === response.id))
                                        .comments
                                        .find((c) => c.id === response.id)
                                        .body = response.body;
                                this.cachedPosts
                                        .find((x) => x.comments.find((c) => c.id === response.id))
                                        .comments
                                        .find((c) => c.id === response.id)
                                        .body = response.body;
                                this.toasterService.showSuccess('Comment edited', 'Comment');
                            });
    }

    public getPosts() {
        this.loadingPosts = true;
        this.postService
            .getPosts()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    this.loadingPosts = false;
                    console.log(resp.body);
                    this.posts = this.cachedPosts = resp.body;
                },
                (error) => (this.loadingPosts = false)
            );
    }

    public sendPost() {
        const postSubscription = !this.imageFile
            ? this.postService.createPost(this.post)
            : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
                  switchMap((imageData) => {
                      this.post.previewImage = imageData.body.data.link;
                      return this.postService.createPost(this.post);
                  })
              );

        this.loading = true;

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
            (respPost) => {
                this.addNewPost(respPost.body);
                this.removeImage();
                this.post.body = undefined;
                this.post.previewImage = undefined;
                this.loading = false;
            },
            (error) => this.snackBarService.showErrorMessage(error)
        );

        this.toasterService.showSuccess('Post created', 'Post');
    }

    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.imageUrl = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }

    public removeImage() {
        this.imageUrl = undefined;
        this.imageFile = undefined;
    }

    public sliderChanged(event: MatSlideToggleChange) {
        this.displayOnlyMine(event.checked);
    }

    public displayOnlyMine(checked: boolean) {
        if (checked) {
            this.isOnlyMine = true;
            this.posts = this.posts.filter((x) => x.author.id === this.currentUser.id);
        } else {
            this.posts = this.cachedPosts;
            this.isOnlyMine = false;
            if (this.isLikedByMe) {
                this.displayPostsLikedByMe(true);
            }
        }
    }

    public sliderChangedLiked(event: MatSlideToggleChange) {
        this.displayPostsLikedByMe(event.checked);
    }

    public displayPostsLikedByMe(checked: boolean) {
        if (checked) {
            this.posts = this.posts
            .filter((x) =>
                (x.reactions
                    .filter(
                        (c) => c.isLike && c.user.id === this.currentUser.id).length !== 0
                    )
            );
            this.isLikedByMe = true;
        } else {
            this.posts = this.cachedPosts;
            this.isLikedByMe = false;
            if (this.isOnlyMine) {
                this.displayOnlyMine(true);
            }
        }

    }

    public toggleNewPostContainer() {
        this.showPostContainer = !this.showPostContainer;
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public registerHub() {
        this.postHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/post').build();
        this.postHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.postHub.on('NewPost', (newPost: Post) => {
            if (newPost) {
                this.addNewPost(newPost);
            }
        });

        this.postHub.on('DeletePost', (postToDelete: Post) => {
            if (postToDelete) {
                this.posts = this.posts.filter((x) => x.id !== postToDelete.id);
                this.cachedPosts = this.cachedPosts.filter((x) => x.id !== postToDelete.id);
                this.toasterService.showSuccess('Post deleted', 'Post');
            }
        });

        this.commentHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/comment').build();
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub.on('NewComment', (commentNew: CommentSignalR) => {
            if (commentNew) {
                if (commentNew.author.id !== this.currentUser.id) {
                    console.log(commentNew);
                    this.comment.id = commentNew.id;
                    this.comment.author = commentNew.author;
                    this.comment.createdAt = commentNew.createdAt;
                    this.comment.body = commentNew.body;
                    this.comment.reactions = [];
                    this.cachedPosts
                        .find((x) => x.id === commentNew.postId).comments = this.cachedPosts
                                                                            .find((x) => x.id === commentNew.postId)
                                                                            .comments
                                                                            .concat(this.comment);
                }
            }
        });

        this.commentHub.on('DeleteComment', (commentToDelete: CommentSignalR) => {
            if (commentToDelete) {
                this.comment.id = commentToDelete.id;
                this.comment.author = commentToDelete.author;
                this.comment.createdAt = commentToDelete.createdAt;
                this.comment.body = commentToDelete.body;
                this.comment.reactions = [];
                this.posts.find((x) => x.id === commentToDelete.postId).comments =
                this.posts.find((x) => x.id === commentToDelete.postId)
                                    .comments.filter((x) => x.id !== commentToDelete.id);
                this.cachedPosts.find((x) => x.id === commentToDelete.postId).comments =
                this.cachedPosts.find((x) => x.id === commentToDelete.postId)
                                    .comments.filter((x) => x.id !== commentToDelete.id);
                this.toasterService.showSuccess('Comment deleted', 'Comment');
            }
        });

    }

    public registerReactionHub() {
        this.reactionHub = new HubConnectionBuilder().withUrl('https://localhost:44344/notifications/reaction').build();
        this.reactionHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.reactionHub.on('NewReaction', (newPost: Post) => {
            if (newPost) {
                const check = this.posts.find((x) => x.id === newPost.id);
                check.reactions = newPost.reactions;
                this.cachedPosts.find((x) => x.id === newPost.id).reactions = newPost.reactions;
                this.signalRService.postReactionsEdited.emit(newPost);
            }
        });

        this.reactionHub.on('NewCommentReaction', (newComment: CommentReactionSignalr) => {
            if (newComment) {
                console.log(newComment.reactions);
                this.posts.find((x) => x.id === newComment.postId).comments
                            .find((a) => a.id === newComment.id).reactions      = newComment.reactions;
                this.cachedPosts.find((x) => x.id === newComment.postId).comments
                                .find((a) => a.id === newComment.id).reactions      = newComment.reactions;
                this.signalRService.commentReactionsEdited.emit(true);
            }
        });
    }

    public addNewPost(newPost: Post) {
        if (!this.cachedPosts.some((x) => x.id === newPost.id)) {
            this.cachedPosts = this.sortPostArray(this.cachedPosts.concat(newPost));
            if (!this.isOnlyMine || (this.isOnlyMine && newPost.author.id === this.currentUser.id)) {
                this.posts = this.sortPostArray(this.posts.concat(newPost));
            }
        }
    }
    public onPostDeleted(id: number) {
        const deletedPost = this.posts.find((x) => x.id === id);
        this.posts = this.posts.filter((item) => item !== deletedPost);
        this.cachedPosts = this.posts;
        this.toasterService.showSuccess('Post deleted', 'Post');
    }

    public onPostEdited(post: Post) {
        this.posts.find((x) => x.id === post.id).body = post.body;
        this.posts.find((x) => x.id === post.id).previewImage = post.previewImage;
        this.cachedPosts = this.posts;
        this.toasterService.showSuccess('Post edited', 'Post');
    }

    private getUser() {
        this.authService
            .getUser()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user) => (this.currentUser = user));
    }

    private sortPostArray(array: Post[]): Post[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
