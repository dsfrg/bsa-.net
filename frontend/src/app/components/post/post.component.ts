import { Component, Input, OnDestroy, Output, EventEmitter, OnInit } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject, from } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { EditPost } from 'src/app/models/post/edit-post';
import { EventService } from 'src/app/services/event.service';
import { emit } from 'cluster';
import { ImgurService } from 'src/app/services/imgur.service';
import { map } from 'rxjs/operators';
import { ReactionService } from 'src/app/services/reaction.service';
import { SharedPostDto } from 'src/app/models/post/shared-post-dto';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { ToasterNotificationService } from 'src/app/services/toaster-notification.service';
import { HubConnectionBuilder, HubConnection } from '@aspnet/signalr';
import { SignalrEventsService } from 'src/app/services/signalr-events.service';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;
    public likeCount: number;
    public dislikeCount: number;

    @Output() public postDeleted: EventEmitter<number> = new EventEmitter();
    @Output() public postEdited: EventEmitter<Post> = new EventEmitter();

    public showComments = false;
    public newComment = {} as NewComment;
    public EditPost: EditPost = {id: 0, Body: '', PreviewImage: ''};
    public SharedPost: SharedPostDto = {postId: -1, userId: -1};
    public editPostClicked = false;

    public imageUrl: string;
    public imageFile: File;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private eventService: EventService,
        private imgurService: ImgurService,
        private reactionDialogService: ReactionService,
        private toasterService: ToasterNotificationService,
        private signalRService: SignalrEventsService
    ) {}

    public ngOnInit() {
        this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
        this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
        this.signalRService.postReactionsEdited
                            .pipe(takeUntil(this.unsubscribe$))
                            .subscribe((response) => {
                                this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
                                this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
                            });
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;
                    console.log(this.post);
                });
            this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
            this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                console.log(this.post);
            });
        this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
        this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
    }

    public dislikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.post = post;
                    console.log(this.post);
                });
            this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
            this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
            return;
        }

        this.likeService
            .dislikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.post = post;
                console.log(this.post);
            });
        this.dislikeCount = this.post.reactions.filter((x) => x.isDislike).length;
        this.likeCount = this.post.reactions.filter((x) => x.isLike).length;
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }

    // tslint:disable-next-line: member-ordering
    public editPost() {
        this.toasterService.showWarning('Post will be edited soon', 'Post');
        const postSubscription = !this.imageFile
        ? this.postService.editPost(this.EditPost)
        : this.imgurService.uploadToImgur(this.imageFile, 'title').pipe(
              switchMap((imageData) => {
                  this.EditPost.PreviewImage = imageData.body.data.link;
                  return this.postService.editPost(this.EditPost);
              })
        );

        postSubscription.pipe(takeUntil(this.unsubscribe$)).subscribe(
        (respPost) => {
            this.postEdited.emit(respPost.body);
            this.removeImage();
        },
            (error) => this.snackBarService.showErrorMessage(error)
        );
        this.editPostClicked = false;
    }
    // tslint:disable-next-line: member-ordering
    public onEditPostClicked() {
        this.editPostClicked = !this.editPostClicked;
        this.EditPost.Body = this.post.body;
        this.EditPost.PreviewImage = this.post.previewImage;
        this.EditPost.id = this.post.id;
    }
    // tslint:disable-next-line: member-ordering
    public loadImage(target: any) {
        this.imageFile = target.files[0];

        if (!this.imageFile) {
            target.value = '';
            return;
        }

        if (this.imageFile.size / 1000000 > 5) {
            target.value = '';
            this.snackBarService.showErrorMessage(`Image can't be heavier than ~5MB`);
            return;
        }

        const reader = new FileReader();
        reader.addEventListener('load', () => (this.EditPost.PreviewImage = reader.result as string));
        reader.readAsDataURL(this.imageFile);
    }
    // tslint:disable-next-line: member-ordering
    public removeImage() {
        this.EditPost.PreviewImage = undefined;
        this.imageFile = undefined;
    }
    // tslint:disable-next-line: member-ordering
    public deletePost() {
        console.log('delete');
        console.log(this.post.id);
        this.postService.deletePost(this.post.id)
                            .pipe(takeUntil(this.unsubscribe$))
                            .subscribe(() => (this.post));

        this.postDeleted.emit(this.post.id);
    }
// tslint:disable-next-line: member-ordering
    public displayWhoLiked() {
        this.postService.displayWhoLiked(this.post.id)
                                        .pipe(takeUntil(this.unsubscribe$))
                                        .subscribe((resp) => {
                                            this.reactionDialogService.openDialog(resp.body, 'Post likes');
                                        });
    }

    // tslint:disable-next-line: member-ordering
    public displayWhoDisliked() {
        this.postService.displayWhoDisliked(this.post.id)
                                        .pipe(takeUntil(this.unsubscribe$))
                                        .subscribe((resp) => {
                                            this.reactionDialogService.openDialog(resp.body, 'Post dislikes');
                                        });
    }
    // tslint:disable-next-line: member-ordering
    public sharePost() {
        console.log(this.post.id);
        this.SharedPost.postId = this.post.id;
        this.SharedPost.userId = this.currentUser.id;
        console.log(this.SharedPost);
        this.postService.sharePostByEmail(this.SharedPost).pipe(takeUntil(this.unsubscribe$)).subscribe(() => (console.log('Subscribe sahre')));
    }
}
