import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EditComment } from 'src/app/models/comment/edit-comment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-comment-edit-dialog',
  templateUrl: './comment-edit-dialog.component.html',
  styleUrls: ['./comment-edit-dialog.component.css']
})
export class CommentEditDialogComponent implements OnInit {

  public updateComment: EditComment = {postId: 0, commentId: 0, Body: ''};
  constructor(
    private dialogRef: MatDialogRef<CommentEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  public ngOnInit() {
    this.updateComment = this.data.data;
  }

  public close() {
    this.dialogRef.close(false);
  }

  public closeAndUpdate() {
    this.dialogRef.close(this.updateComment);
  }

}
