﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Entities;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly PostService _postService;
        private readonly LikeService _likeService;
        private readonly DislikeService _dislikeService;
        private readonly EmailService _emailService;

        public PostsController(PostService postService, LikeService likeService, DislikeService dislikeService, EmailService emailService)
        {
            _postService = postService;
            _likeService = likeService;
            _dislikeService = dislikeService;
            _emailService = emailService;
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ICollection<PostDTO>>> Get()
        {
            var posts = await _postService.GetAllPosts();
            return Ok(posts);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PostDTO>> GetPost(int id)
        {
            return Ok(await _postService.GetPost(id));
        }

        [HttpPost]
        public async Task<ActionResult<PostDTO>> CreatePost([FromBody] PostCreateDTO dto)
        {
            dto.AuthorId = this.GetUserIdFromToken();

            return Ok(await _postService.CreatePost(dto));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _likeService.LikePost(reaction);
            return Ok();
        }

        [HttpPost("dislike")]
        public async Task<IActionResult> DislikePost(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();

            await _dislikeService.DislikePost(reaction);
            return Ok();
        }


        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            if(!await _postService.DeletePost(id))
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpPut("edit")]
        public async Task<IActionResult> EditPost(PostEditDTO postEditDTO)
        {

           var postDto = await _postService.EditPost(postEditDTO);
           return Ok(postDto);
        }

        [HttpGet("likeduser/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUserWhoLikedPost(int id)
        {
            return Ok(await _likeService.GetUserWhoLikedPost(id));
        }


        [HttpGet("dislikeduser/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUserWhoDislikedPost(int id)
        {
            return Ok(await _dislikeService.GetUserWhoDislikedPost(id));
        }


        [HttpGet("likedbyme/{id}")]
        public async Task<IActionResult> GetPostLikedByUser(int id)
        {
            return Ok(await _postService.GetPostLikedByMe(id));
        }

        [HttpPost("share")]
        public async Task<IActionResult> SharePost(PostShareDTO post)
        {
            post.UserId = this.GetUserIdFromToken();
            await _emailService.SharePostByEmail(post);
            return Ok();
        }
    }
}