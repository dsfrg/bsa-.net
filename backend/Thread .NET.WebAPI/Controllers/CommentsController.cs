﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Extensions;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly CommentService _commentService;
        private readonly LikeService _likeService;
        private readonly DislikeService _dislikeService;

        public CommentsController(CommentService commentService, LikeService likeService, DislikeService dislikeService)
        {
            _commentService = commentService;
            _likeService = likeService;
            _dislikeService = dislikeService;
        }

        [HttpPost]
        public async Task<ActionResult<CommentDTO>> CreateComment([FromBody] NewCommentDTO comment)
        {
            comment.AuthorId = this.GetUserIdFromToken();
            return Ok(await _commentService.CreateComment(comment));
        }

        [HttpPost("like")]
        public async Task<IActionResult> LikeComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _likeService.LikeComment(reaction);
            return Ok();
        }

        [HttpPost("dislike")]
        public async Task<IActionResult> DislikeComment(NewReactionDTO reaction)
        {
            reaction.UserId = this.GetUserIdFromToken();
            await _dislikeService.DislikeComment(reaction);
            return Ok();
        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> RemoveComment(int id)
        {
            await _commentService.DeleteComment(id);
            return Ok();
        }

        [HttpPost("update")]
        public async Task<ActionResult<CommentDTO>> UpdateComment([FromBody] UpdateCommentDTO update)
        {
            return Ok(await _commentService.UpdateComment(update));
        }


        [HttpGet("likeduser/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUserWhoLikedComment(int id)
        {
            return Ok(await _likeService.GetUserWhoLikedComment(id));
        }


        [HttpGet("dislikeduser/{id}")]
        [AllowAnonymous]
        public async Task<IActionResult> GetUserWhoDislikedPost(int id)
        {
            return Ok(await _dislikeService.GetUserWhoDislikedComment(id));
        }
    }
}