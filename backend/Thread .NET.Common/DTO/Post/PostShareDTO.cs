﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Post
{
    public class PostShareDTO
    {
        public  int UserId{ get; set; }
        public int PostId { get; set; }
    }
}
