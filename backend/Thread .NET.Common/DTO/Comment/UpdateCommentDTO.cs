﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.Comment
{
    public class UpdateCommentDTO
    {
        public int CommentID { get; set; }
        public int PostID { get; set; }
        public string Body { get; set; }
    }
}
