﻿using System;
using System.Collections.Generic;
using System.Text;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Common.DTO.Comment
{
    public class CommentReactionSignalR
    {
        public int Id { get; set; }
        public UserDTO Author { get; set; }
        public int postId { get; set; }
        public ICollection<ReactionDTO> Reactions { get; set; }
    }
}
