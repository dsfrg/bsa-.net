﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.User
{
    public class UserForgotDTO
    {
        public string Email { get; set; }

        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
