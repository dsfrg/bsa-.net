﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.User
{
    public class UserEmailDTO
    {
        public string Email { get; set; }
    }
}
