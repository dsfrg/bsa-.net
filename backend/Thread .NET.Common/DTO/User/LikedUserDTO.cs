﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Thread_.NET.Common.DTO.User
{
    public class LikedUserDTO
    {
        public string Username { get; set; }
        public string ImgURL { get; set; }
    }
}
