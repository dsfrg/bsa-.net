﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public class EmailService : BaseService
    {
        public EmailService(ThreadContext context, IMapper mapper) : base(context, mapper)
        {
        }

        public async Task SharePostByEmail(PostShareDTO post)
        {
            var sharedPost = await _context.Posts
                                    .Include(x => x.Preview)
                                    .FirstOrDefaultAsync(x => x.Id == post.PostId);

            var userRequestedSharing = await _context.Users.FirstOrDefaultAsync(x => x.Id == post.UserId);

            MailAddress from = new MailAddress("");
            MailAddress to = new MailAddress(userRequestedSharing.Email);

            MailMessage message = new MailMessage(from, to);
            message.Subject = "Shared post";
            message.IsBodyHtml = true;

            message.AlternateViews.Add(Mail_Body(sharedPost.Body, sharedPost.Preview.URL));

            SmtpClient SmtpMail = new SmtpClient("smtp.gmail.com"); 
            SmtpMail.Port = 587;
            
            SmtpMail.Credentials = new System.Net.NetworkCredential("", "!");  
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpMail.EnableSsl = true;
            SmtpMail.ServicePoint.MaxIdleTime = 0;
            SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
            message.BodyEncoding = Encoding.Default;
            message.Priority = MailPriority.High;
            SmtpMail.Send(message);
        }
        private  LinkedResource GetLinkedResource(string path)
        {
            string image = path;
            var webClient = new WebClient();
            byte[] imageBytes = webClient.DownloadData(new Uri(image));
            MemoryStream ms = new MemoryStream(imageBytes);
            LinkedResource resource = new LinkedResource(ms, MediaTypeNames.Image.Jpeg);
            return resource;
        }
        private AlternateView Mail_Body(string body, string url)
        {


            string path = url;
            LinkedResource Img = GetLinkedResource(path);
            Img.ContentId = "MyImage";
            string str = @"  
            <table>  
                <tr>  
                    <td> '" + body + @"'  
                    </td>  
                </tr>  
                <tr>  
                    <td>  
                      <img src=cid:MyImage  id='img' alt='' width='100px' height='100px'/>   
                    </td>  
                </tr>
            </table>  
            ";
            AlternateView AV =  AlternateView.CreateAlternateViewFromString(str, null, MediaTypeNames.Text.Html);
            AV.LinkedResources.Add(Img);
            return AV;
        }
    }
}
