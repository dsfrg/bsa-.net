﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class PostService : BaseService
    {
        private readonly IHubContext<PostHub> _postHub;

        public PostService(ThreadContext context, IMapper mapper, IHubContext<PostHub> postHub) : base(context, mapper)
        {
            _postHub = postHub;
        }

        public async Task<ICollection<PostDTO>> GetAllPosts()
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Reactions)
                    .ThenInclude(reaction => reaction.User)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Reactions)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .OrderByDescending(post => post.CreatedAt)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> GetPost(int id)
        {
            var post = await _context.Posts
                .Include(x => x.Author)
                    .ThenInclude(x => x.Avatar)
                .Include(x => x.Preview)
                .Include(x => x.Reactions)
                    .ThenInclude(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Reactions)
                        .ThenInclude(x => x.User)
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .FirstOrDefaultAsync(x => x.Id == id);

            return _mapper.Map<PostDTO>(post);
        }

        public async Task<ICollection<PostDTO>> GetAllPosts(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Where(p => p.AuthorId == userId)
                .ToListAsync();

            return _mapper.Map<ICollection<PostDTO>>(posts);
        }

        public async Task<PostDTO> CreatePost(PostCreateDTO postDto)
        {
            var postEntity = _mapper.Map<Post>(postDto);

            _context.Posts.Add(postEntity);
            await _context.SaveChangesAsync();

            var createdPost = await _context.Posts
                .Include(post => post.Author)
					.ThenInclude(author => author.Avatar)
                .FirstAsync(post => post.Id == postEntity.Id);

            var createdPostDTO = _mapper.Map<PostDTO>(createdPost);
            await _postHub.Clients.All.SendAsync("NewPost", createdPostDTO);

            return createdPostDTO;
        }

        public async Task<bool> DeletePost(int id)
        {        
             var post = _context.Posts.Where(x => x.Id == id).FirstOrDefault();
            await _postHub.Clients.All.SendAsync("DeletePost", _mapper.Map<PostDTO>(post));
            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            
            return true;
        }

        public async Task<PostDTO> EditPost(PostEditDTO postEditDTO)
        {
            var post = await _context.Posts.Include(post => post.Preview)
                    .FirstOrDefaultAsync(x => x.Id == postEditDTO.Id);
            if (post.Preview != null)
                post.Preview.URL = postEditDTO.PreviewImage;
            else post.Preview = new Image() { 
                                            URL = postEditDTO.PreviewImage, 
                                            CreatedAt = DateTime.Now, 
                                            UpdatedAt = DateTime.Now 
                                        };

            post.Body = postEditDTO.Body;

            _context.Posts.Update(post);
            await _context.SaveChangesAsync();
            return _mapper.Map<PostDTO>(post);
        }


        public async Task<List<PostDTO>> GetPostLikedByMe(int userId)
        {
            var posts = await _context.Posts
                .Include(post => post.Author)
                    .ThenInclude(author => author.Avatar)
                .Include(post => post.Preview)
                .Include(post => post.Comments)
                    .ThenInclude(comment => comment.Author)
                .Include(x => x.Reactions)
                    .ThenInclude(x => x.User)
                .Where(p => p.AuthorId != userId && p.Reactions.FirstOrDefault(x => x.UserId == userId && x.IsLike && x.PostId == p.Id) != null)
                .ToListAsync();
            return _mapper.Map<List<PostDTO>>(posts);
        }
    }
}
