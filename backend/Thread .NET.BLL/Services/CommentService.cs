﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _hubContext;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> hubContext) : base(context, mapper) 
        {
            _hubContext = hubContext;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            var createdCommentDTO = _mapper.Map<CommentDTO>(createdComment);

            var newCommentDTO = new CommentSignalRDTO() 
            {  
                Id = createdComment.Id,
                PostId = createdComment.PostId,
                CreatedAt = DateTime.Now,
                Author = createdCommentDTO.Author,
                Body = newComment.Body,
                Reactions = null
            };
            await _hubContext.Clients.All.SendAsync("NewComment", newCommentDTO);

            return createdCommentDTO;
        }

        public async Task DeleteComment(int id)
        {
            var commentReactions = _context.CommentReactions.Where(x => x.CommentId == id );
            var comment = await _context.Comments.FirstOrDefaultAsync(x => x.Id == id);
            var newCommentDTO = new CommentSignalRDTO()
            {
                Id = comment.Id,
                PostId = comment.PostId,
                CreatedAt = DateTime.Now,
                Author = _mapper.Map<UserDTO>(comment.Author),
                Body = comment.Body,
                Reactions = null
            };

            if (commentReactions.Any())
            {
                _context.CommentReactions.RemoveRange(commentReactions);
                _context.Comments.Remove(comment);
                await _context.SaveChangesAsync();
                await _hubContext.Clients.All.SendAsync("DeleteComment", newCommentDTO);
                return;
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();
            await _hubContext.Clients.All.SendAsync("DeleteComment", newCommentDTO);
        }

        public async Task<CommentDTO> UpdateComment(UpdateCommentDTO update)
        {
            var comment = await _context.Comments
                .Include(x => x.Author)
                    .ThenInclude(x => x.Avatar)
                .Include(x => x.Reactions)
                .FirstOrDefaultAsync(x => x.Id == update.CommentID && x.PostId == update.PostID);

            comment.Body = update.Body;

            _context.Comments.Update(comment);
            await _context.SaveChangesAsync();

            return _mapper.Map<CommentDTO>(comment);
        }

    }
}
