﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Common.Security;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class UserService : BaseService
    {
        public UserService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task<ICollection<UserDTO>> GetUsers()
        {
            var users = await _context.Users
                .Include(x => x.Avatar)
                .ToListAsync();

            return _mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var user = await GetUserByIdInternal(id);
            if (user == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            return _mapper.Map<UserDTO>(user);
        }

        public async Task<UserDTO> CreateUser(UserRegisterDTO userDto)
        {
            var checkUserUsername = await _context.Users.FirstOrDefaultAsync(x => x.UserName == userDto.UserName);
            var checkUserEmail = await _context.Users.FirstOrDefaultAsync(x => x.Email == userDto.Email);
            if( checkUserEmail != null || checkUserUsername != null)
            {
                return null;
            }

            var userEntity = _mapper.Map<User>(userDto);
            var salt = SecurityHelper.GetRandomBytes();

            userEntity.Salt = Convert.ToBase64String(salt);
            userEntity.Password = SecurityHelper.HashPassword(userDto.Password, salt);

            _context.Users.Add(userEntity);
            await _context.SaveChangesAsync();

            return _mapper.Map<UserDTO>(userEntity);
        }

        public async Task UpdateUser(UserDTO userDto)
        {
            var userEntity = await GetUserByIdInternal(userDto.Id);
            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userDto.Id);
            }

            var timeNow = DateTime.Now;

            userEntity.Email = userDto.Email;
            userEntity.UserName = userDto.UserName;
            userEntity.UpdatedAt = timeNow;

            if (!string.IsNullOrEmpty(userDto.Avatar))
            {
                if (userEntity.Avatar == null)
                {
                    userEntity.Avatar = new Image
                    {
                        URL = userDto.Avatar
                    };
                }
                else
                {
                    userEntity.Avatar.URL = userDto.Avatar;
                    userEntity.Avatar.UpdatedAt = timeNow;
                }
            }
            else
            {
                if (userEntity.Avatar != null)
                {
                    _context.Images.Remove(userEntity.Avatar);
                }
            }

            _context.Users.Update(userEntity);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteUser(int userId)
        {
            var userEntity = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userId);
            }

            _context.Users.Remove(userEntity);
            await _context.SaveChangesAsync();
        }

        private async Task<User> GetUserByIdInternal(int id)
        {
            return await _context.Users
                .Include(u => u.Avatar)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<bool> SendResetPasswordLink(string email)
        {
            var user = await _context.Users.FirstOrDefaultAsync(x => x.Email == email);
            if(user == null)
            {
                return false;
            }
            var callbackUrl = $"http://localhost:4200/reset?email={email}";

            SendEmailAsync(email,
                $"Follow <a href='{callbackUrl}'>link</a> to reset password ");
            return true;
        }

        public async Task<bool> ResetPassword(UserForgotDTO userForgotDTO)
        {
            var userEntity = await _context.Users.FirstOrDefaultAsync(x => x.Email == userForgotDTO.Email);

            if (userEntity == null)
            {
                return false;
            }

            if (!SecurityHelper.ValidatePassword(userForgotDTO.OldPassword, userEntity.Password, userEntity.Salt))
            {
                return false;
            }

            var salt = SecurityHelper.GetRandomBytes();

            userEntity.Salt = Convert.ToBase64String(salt);
            userEntity.Password = SecurityHelper.HashPassword(userForgotDTO.NewPassword, salt);

            _context.Users.Update(userEntity);
            await _context.SaveChangesAsync();
            return true;
        }

        public void SendEmailAsync(string email, string body)
        {
            
            MailAddress from = new MailAddress("");
            MailAddress to = new MailAddress(email);

            MailMessage message = new MailMessage(from, to);
            message.IsBodyHtml = true;
            message.Subject = "Reset password";
            message.Body = body;
            SmtpClient SmtpMail = new SmtpClient("smtp.gmail.com");
            SmtpMail.Port = 587;
            SmtpMail.Credentials = new System.Net.NetworkCredential("", "");
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpMail.EnableSsl = true;
            SmtpMail.ServicePoint.MaxIdleTime = 0;
            SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
            message.BodyEncoding = Encoding.Default;
            message.Priority = MailPriority.High;

            SmtpMail.Send(message);
        }
    }
}
