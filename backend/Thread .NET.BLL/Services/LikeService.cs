﻿using AutoMapper;
using Bogus.Extensions;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        private readonly IHubContext<ReactionHub> _hubContext;
        public LikeService(ThreadContext context, IMapper mapper, IHubContext<ReactionHub> hubContext) : base(context, mapper) 
        {
            _hubContext = hubContext;
        }

        public async Task LikePost(NewReactionDTO newReaction)
        {
            var reaction = await _context.PostReactions.FirstOrDefaultAsync(x => x.PostId == newReaction.EntityId && x.UserId == newReaction.UserId);
            var newPostReaction = new PostReaction()
            {
                PostId = newReaction.EntityId,
                UserId = newReaction.UserId,
                IsLike = newReaction.IsLike,
                IsDislike = newReaction.IsDislike
            };

            if (reaction != null && reaction.IsDislike)
            {
                _context.PostReactions.RemoveRange(reaction);
                await _context.SaveChangesAsync();
                _context.PostReactions.Add(newPostReaction);
                await _context.SaveChangesAsync();

            }
            else if (reaction != null)
            {
                _context.PostReactions.RemoveRange(reaction);
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.PostReactions.Add(newPostReaction);
                await _context.SaveChangesAsync();
            }

            var likedPost = await _context.Posts
                                    .Include(post => post.Author)
                                        .ThenInclude(author => author.Avatar)
                                    .Include(x => x.Reactions)
                                        .ThenInclude(x => x.User)
                                            .ThenInclude(x => x.Avatar)
                                    .FirstOrDefaultAsync(x => x.Id == newReaction.EntityId);

            var newPostDTO = _mapper.Map<PostDTO>(likedPost);
            await _hubContext.Clients.All.SendAsync("NewReaction", newPostDTO);


            await NotifyByEmail(newReaction.EntityId, newReaction.UserId);
        }

        public async Task<List<LikedUserDTO>> GetUserWhoLikedPost(int postID)
        {
            var postPeactions = await _context.PostReactions
                                        .Include(x => x.User)
                                            .ThenInclude(x => x.Avatar)
                                        .Where(x => x.PostId == postID && x.IsLike).ToListAsync();

            List<LikedUserDTO> likedUser = new List<LikedUserDTO>();
            foreach(var item in postPeactions)
            {
                var user = new LikedUserDTO()
                {
                    Username = item.User.UserName,
                    ImgURL = item.User.Avatar.URL
                };
                likedUser.Add(user);
            }
            return likedUser;
        }

        private async Task NotifyByEmail(int postId, int userId)
        {
            var post = await _context.Posts.Include(x => x.Author).FirstOrDefaultAsync(x => x.Id == postId);
            var personWhoLiked = await _context.Users.FirstOrDefaultAsync(x => x.Id == userId);
            MailAddress from = new MailAddress("");
            MailAddress to = new MailAddress(post.Author.Email);

            MailMessage message = new MailMessage(from, to);
            message.Subject = "Shared post";
            message.Body = $"{personWhoLiked.UserName} with email: {personWhoLiked.Email} liked your post";

            SmtpClient SmtpMail = new SmtpClient("smtp.gmail.com");
            SmtpMail.Port = 587;
            SmtpMail.Credentials = new System.Net.NetworkCredential("", "");
            SmtpMail.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpMail.EnableSsl = true;
            SmtpMail.ServicePoint.MaxIdleTime = 0;
            SmtpMail.ServicePoint.SetTcpKeepAlive(true, 2000, 2000);
            message.BodyEncoding = Encoding.Default;
            message.Priority = MailPriority.High;

            SmtpMail.Send(message);
        }

        public async Task<List<LikedUserDTO>>GetUserWhoLikedComment(int id)
        {
            var postPeactions = await _context.CommentReactions
                                        .Include(x => x.User)
                                            .ThenInclude(x => x.Avatar)
                                        .Where(x => x.CommentId == id && x.IsLike).ToListAsync();

            List<LikedUserDTO> likedUser = new List<LikedUserDTO>();
            foreach (var item in postPeactions)
            {
                var user = new LikedUserDTO()
                {
                    Username = item.User.UserName,
                    ImgURL = item.User.Avatar.URL
                };
                likedUser.Add(user);
            }

            return likedUser;
        }

        public async Task LikeComment(NewReactionDTO newReaction)
        {
            var reaction = await _context.CommentReactions.FirstOrDefaultAsync(x => x.CommentId == newReaction.EntityId && x.UserId == newReaction.UserId);
            var newPostReaction = new CommentReaction()
            {
                CommentId = newReaction.EntityId,
                UserId = newReaction.UserId,
                IsLike = newReaction.IsLike,
                IsDislike = newReaction.IsDislike
            };
            if (reaction != null && reaction.IsDislike)
            {
                _context.CommentReactions.RemoveRange(reaction);
                await _context.SaveChangesAsync();
                _context.CommentReactions.Add(newPostReaction);
                await _context.SaveChangesAsync();

            }
            else if (reaction != null)
            {
                _context.CommentReactions.RemoveRange(reaction);
                await _context.SaveChangesAsync();
            }
            else
            {
                _context.CommentReactions.Add(newPostReaction);
                await _context.SaveChangesAsync();
            }

            var a = (await GetReactionsSignalR(newReaction));
            await _hubContext.Clients.All.SendAsync("NewCommentReaction", a);
        }

        private async Task<CommentReactionSignalR> GetReactionsSignalR(NewReactionDTO newReaction)
        {
            var user = await _context.Users.Include(x => x.Avatar).FirstOrDefaultAsync(x => x.Id == newReaction.UserId);
            var post = await _context.Posts
                                        .Include(x => x.Comments)
                                            .ThenInclude(x => x.Reactions)
                                        .FirstOrDefaultAsync(x => x.Comments.FirstOrDefault(c => c.Id == newReaction.EntityId) != null);
            var comment = _context.Comments
                                   .Include(x => x.Reactions)
                                        .ThenInclude(x => x.User)
                                            .ThenInclude(x => x.Avatar)
                                   .FirstOrDefault(x => x.Id == newReaction.EntityId);
            var reactions = new CommentReactionSignalR()
            {
                Id = newReaction.EntityId,
                Author = _mapper.Map<UserDTO>(user),
                postId = post.Id,
                Reactions = _mapper.Map<ICollection<ReactionDTO>>(comment.Reactions)
            };


            return reactions;
        }
    }
}
