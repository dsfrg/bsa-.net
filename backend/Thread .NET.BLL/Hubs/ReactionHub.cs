﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.Common.DTO.Post;

namespace Thread_.NET.BLL.Hubs
{
    public class ReactionHub : Hub
    {
        public async Task SendReaction(PostDTO post)
        {
            await Clients.All.SendAsync("NewReaction", post);
        }

        public async Task SendCommentReactions(CommentReactionSignalR comment)
        {
            await Clients.All.SendAsync("NewCommentReaction", comment);
        }
    }
}
