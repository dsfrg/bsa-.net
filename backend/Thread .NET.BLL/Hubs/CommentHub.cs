﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Hubs
{
    public class CommentHub : Hub
    {
        public async Task Send(CommentSignalRDTO comment)
        {
            await Clients.All.SendAsync("NewComment", comment);
        }

        public async Task Delete(CommentSignalRDTO comment)
        {
            await Clients.All.SendAsync("DeleteComment", comment);
        }
    }
}
